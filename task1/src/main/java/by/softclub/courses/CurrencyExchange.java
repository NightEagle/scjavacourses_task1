package by.softclub.courses;

import java.util.List;

public class CurrencyExchange {
    private final List<CurrencyConverter> converters;

    public CurrencyExchange(List<CurrencyConverter> converters) {
        this.converters = converters;
    }

    public double exchange(Car car, Currency currency) throws Exception {
        for (CurrencyConverter converter : converters) {
            if (converter.getCurrency().equals(currency)) {
                return converter.convert(car.getPrice());
            }
        }

        throw new Exception("Converter not found for currency " + currency.toString());
    }
}
