package by.softclub.courses;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.management.remote.rmi._RMIConnection_Stub;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Runner {

    public static void main(String[] args) throws Exception {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

        CurrencyExchange exchange = context.getBean("currencyExchange", CurrencyExchange.class);

        Car car = Car.valueOf(args[0]);
        Currency currency = Currency.valueOf(args[1]);

        String result = currency.toString() + " " + exchange.exchange(car, currency);

        if (args.length > 2) {
            String filename = args[2] + "result.txt";

            Files.write(Paths.get(filename), result.getBytes());
        } else {
            System.out.println(result);
        }

        context.close();
    }
}