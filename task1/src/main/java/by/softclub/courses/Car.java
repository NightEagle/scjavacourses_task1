package by.softclub.courses;

public enum Car {
    BMW(10000),
    Audi(20000),
    Subaru(15000);

    private double price;

    Car(double price) {
        this.price = price;
    }

    public double getPrice() {
        return this.price;
    }
}
