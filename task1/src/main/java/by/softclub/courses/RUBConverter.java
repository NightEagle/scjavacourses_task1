package by.softclub.courses;

public class RUBConverter implements CurrencyConverter {

    @Override
    public Currency getCurrency() {
        return Currency.RUB;
    }

    @Override
    public double convert(double sum) {
        return sum * 0.3;
    }
}
