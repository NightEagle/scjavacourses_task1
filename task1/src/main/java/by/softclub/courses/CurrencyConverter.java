package by.softclub.courses;

public interface CurrencyConverter {
    Currency getCurrency();

    double convert(double sum);
}
