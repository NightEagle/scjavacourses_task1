package by.softclub.courses;

public class EURConverter implements CurrencyConverter {

    @Override
    public Currency getCurrency() {
        return Currency.EUR;
    }

    @Override
    public double convert(double sum) {
        return sum / 2.3;
    }
}
