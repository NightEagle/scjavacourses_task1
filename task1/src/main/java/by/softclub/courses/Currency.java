package by.softclub.courses;

public enum Currency {
    RUB,
    USD,
    EUR
}
