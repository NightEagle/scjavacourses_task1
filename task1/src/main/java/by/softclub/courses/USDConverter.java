package by.softclub.courses;

public class USDConverter implements CurrencyConverter {

    @Override
    public Currency getCurrency() {
        return Currency.USD;
    }

    @Override
    public double convert(double sum) {
        return sum / 2;
    }
}
